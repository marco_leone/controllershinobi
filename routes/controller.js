const express = require('express');
const cp = require('child_process');
const router = express.Router();
const conf = require('../conf.json');


const bb = require('bluebird');
const db = require('../my_modules/db');
const m = require('../my_modules/monitoring');
const utils = require('../my_modules/utils');



router.get('/init', (req, res) => { //path reale: localhost/controller/test
    //TODO: spostare invocazione init in app.js
    m.init();
    res.send("prometheus started");
});

/*
FUNZIONE TEST - da rimuovere successivamente
 */
router.post('/testAlert', (req, res) => { //path reale: localhost/controller/testAlert
    //m.init();
    //res.send("prometheus started");
    console.log(req.body);
});


router.get('/testHandleAlert', (req, res, next) => {

    //const api = "yelBYmCeesW9vsxnikxQZGTYk5doWA"; //OK - utente ccio@ è superadmin
    //const api = "Mn9WPTt1V4nwm3aRBtO8Nfg7kN2zfS" //controller@test.it - non funziona perchè utente NON superadmin

    const controllerHost = "http://localhost";
    const shinobiHost= "http://localhost";
    //const nodeShinobiHost = "http://localhost" + conf.shinobiNodePort;

    let deletedMonitor;

    const errHandler = function(err) {
        console.log("ERRORE Promises:" + err.statusCode + ": " + err.message);
        next(new Error("Errore in testHandleAlert: " + err.statusCode + ": " + err.message));
    }

    const startAt = process.hrtime();
     /*
        TODO per ogni alert:
        1. individua alert verificato (switch sui vari tipi)
        2. individua nodo che ha generato alert (alert[i].labels.instance) [variabile shinobiHost/busyHost]
        3. in base all'alert interroga il nodo per ottenere processo che sta consumando più memoria o cpu
    */

     //3. preleva API
    //TODO: preleva API da db locale, se non esiste aggiungi getApiPromise a catena di promises
    //let api = "yelBYmCeesW9vsxnikxQZGTYk5doWA";


    //TODO: inizializzare busyHost con host che ha generato alert
    let busyHost = "http://" + utils.extractHostname(shinobiHost);



    let ke, mid, freeHost, api;
    // 3. preleva API Key (da db locale o da db remoto)
    // 4. estrai monitor id - metodo remoto
    // 5. invoca metodo getFreeNode per ottenere il nodo meno carico
    let getMonitorPromise = bb.join(getAPI(busyHost, req),
            getProcessInfo(busyHost, req),
            getFreeNode(controllerHost, req),
            function (returnedApi, processInfo, freeNode) {
                if(utils.isEmptyString(returnedApi)
                    || utils.isEmptyObject(processInfo)
                    || freeNode.length===0) {

                    throw new Error("Empty response");
                }

                //gestisci risultato ottenuto da getApi()
                api = returnedApi;

                //gestisci risultato ottenuto da /monitoring/getFreeNode
                freeHost = "http://" + utils.extractHostname(freeNode[0].instance); //TODO da verificare

                //gestisci risultato ottenuto da /process/getProcessInfo
                ke = processInfo.ke;
                mid = processInfo.mid;

                // 6. preleva monitor da nodo carico (Shinobi API) [NB. video non viene cancellato]
                return getJsonMonitor(busyHost, api, ke, mid, req)
            }
        ).catch(SyntaxError, function (syntaxError) {
            console.log("Syntax Error" + syntaxError);
            next(syntaxError);
    }).catch(function (err) {
            console.log("Generic error" + err);
            next(err);
    });

    getMonitorPromise.then(function (jsonMonitor) {
        console.log(jsonMonitor);
        // 7. invia monitor a nodo libero (Shinobi API)
        //TODO: concatenato "_api" a mid per test locale - da rimuovere successivamente
        jsonMonitor.mid = jsonMonitor.mid + "_api";
        return addMonitor(freeHost, jsonMonitor, api, ke, mid + "_api", req);
    }).then(function (shinobiAddResponse) {
        if(shinobiAddResponse.ok === false) {
            /*risposta da Shinobi:
            * {
               "ok": true | false,
               "msg": "Monitor Added by user. : XDf5hB3"
            }*/
            let err = new Error("Error in Shinobi: UNABLE TO CREATE MONITOR");
            err.statusCode = 500;
            throw err;
        }
        return deleteMonitor(busyHost, api, ke, mid, req);
    }).then(function (shinobiDelResponse) {
        // 8. stop/cancella monitor su nodo carico (Shinobi API)
        utils.benchmark(startAt, null, "ms", 3, req.route.path, "END REQUEST - Handled Alert", "true");
        if(shinobiDelResponse.ok === false) {
            let err = new Error("Error in Shinobi: UNABLE TO CREATE MONITOR");
            err.statusCode = 500;
            throw err;
        }
        res.send("Handled Alert - api: " + api + "\n DELETED: " + mid);
    }).catch(function (syntaxError) {
        console.log("Syntax Error" + syntaxError);
        next(syntaxError);
    }).catch(function (err) {
        console.log("Generic error" + err);
        next(err);
    });

});

router.get('/testQuery', (req, res, next) => {
    /*let query = "INSERT INTO public.api_cache(ke, uid, api) VALUES ($1, $2, $3) RETURNING *";
    let values = ['pippo', 'pluto', 'topolino'];

    db.getConnection(query, values, function (result) {
        console.log(result[0]);
        res.send(result);
    });*/

    /*let api = getAPI("http://localhost", req);
    api.then(function (result) {
        res.send("ok - api: " + result);
    }).catch(function (err) {
       next(err);
    });*/

    let values = ["http://localhost:8080/yelBYmCeesW9vsxnikxQZGTYk5doWA/configureMonitor/2Df5hBE/6MQFvoEOml_1_api_api_api/delete",
        "7.294", "ms"];
    let query = "INSERT INTO " + db.timeBenchmarkTable + "(from_url, value, unit)"
        + " VALUES ($1, $2, $3) RETURNING *";

    db.getConnection(query, values, function (result) {
        console.log(result[0]);
        res.send(result);
    }).catch(function (err) {
        next(err);
    })

})

router.get('/testPost', (req, res, next) => {
    let options = {
        uri: 'http://localhost:3030/process/getMonitorProcessInfo',
        method: 'POST',
        json: {
            "orderBy": "cpu"
        }
    };

    let testPromise = utils.handleJsonReq(options, req.route.path);

    testPromise.then(function(result) {
        if(utils.isEmptyObject(result.body)) {
            //TODO: gestire errore
        }
        res.send(result.body);
    }, function(err) {
        next(err);
    });

    //;
})

/*
 * Funzione di callback invocata quando si verifica alert
 */
router.post('/handleAlert', (req, res) => {
    /* per ogni alert:
    1. individua alert verificato (switch sui vari tipi)
    2. individua nodo che ha generato alert (alert[i].labels.instance)
    3. in base all'alert interroga il nodo per ottenere processo che sta consumando più memoria o cpu
    4. estrai monitor id
    5. invoca metodo getFreeNode (DA IMPLEMENTARE) per ottenere il nodo meno carico
    6. preleva monitor da nodo carico (Shinobi API) [NB. video non viene cancellato]
    7. invia monitor a nodo libero (Shinobi API)
    8. stop/cancella monitor in nodo carico
     */
    //TODO versione definitiva conterrà codice presente in testHandleAlert
    var alerts = req.body.alerts;
    for(var i=0, len = alerts.length; i<len; i++) {
        //var a = alerts[i].labels.alertname
        switch (alerts[i].labels.alertname) {
            case 'HighProcsRunning':
                handleAlert(alerts[i].labels.instance);
                break;
        }
    }

})



/* PRIVATE FUNCTIONS */
function handleAlert(node, alert) {
    var mid = getMonitorProcessInfo('cpu');
    if (mid==='') {
        //TODO: lancia eccezione
    }

    var host = m.getFreeNode();

/*    6. preleva monitor da nodo carico (Shinobi API) [NB. video non viene cancellato]
    7. invia monitor a nodo libero (Shinobi API)
    8. stop/cancella monitor in nodo carico*/

}




//TODO
/**
 * Funzione per la creazione delle url per l'invocazione delle API di Shinobi
 * @param ke
 * @param mid
 */
function createShinobiAPIUrl(action, api, ke, mid, jsonMonitor) {

    let url="";

    if(utils.isEmptyString(api) || utils.isEmptyString(ke) || utils.isEmptyString(mid)) {
        throw new Error("Missing parameters in createShinobiAPIUrl!");
    }

    switch (action.toLowerCase()) {
        case "getmonitor":
            // http://xxx.xxx.xxx.xxx/[API KEY]/monitor/[GROUP KEY]/[MONITOR ID]
            url = `/${api}/monitor/${ke}/${mid}`;
            break;
        case "addmonitor":
            // http://xxx.xxx.xxx.xxx/[API KEY]/configureMonitor/[GROUP KEY]/[MONITOR ID]/add?data={"mid":..}
            if(jsonMonitor===null || jsonMonitor===undefined || utils.isEmptyObject(jsonMonitor)) {
                throw new Error("Missing data in createShinobiAPIUrl!");
            }
            url = `/${api}/configureMonitor/${ke}/${mid}/add?data=${JSON.stringify(jsonMonitor)}`;
            break;
        case "deletemonitor":
            // http://xxx.xxx.xxx.xxx/[API KEY]/configureMonitor/[GROUP KEY]/[MONITOR ID]/delete
            url = `/${api}/configureMonitor/${ke}/${mid}/delete`;
            break;
        default:
            throw new Error("Action not valid!");
    }

    return url;
}

function getAPI(busyHost, req) {

    let query = "SELECT api FROM " + db.apiTable + " WHERE ke=$1 AND uid=$2";
    let values = [conf.shinobiControllerAccount.ke, conf.shinobiControllerAccount.uid];

    //TODO: refactoring per restituire promessa

    return new Promise(function (resolve, reject) {
        db.getConnection(query, values, function (result) {
            if(result.rowCount !== 0) {
                //TODO check risultato
                console.log("API prelevata dal database: " + result);
                resolve(result.rows[0].api);
            }
            else {
                //preleva API da remoto
                const getAPIOptions = {
                    uri: busyHost + conf.shinobiNodePort + '/shinobi/getApiKey',
                    method: 'POST',
                    json: {
                        "ke": conf.shinobiControllerAccount.ke,
                        "uid": conf.shinobiControllerAccount.uid
                    }
                };

                let getApiPromise = utils.handleJsonReq(getAPIOptions, req.route.path);

                getApiPromise.then(function (res) {
                    let query = "INSERT INTO " + db.apiTable + "(ke, uid, ip, api, details)"
                        + "VALUES ($1, $2, $3, $4, $5) RETURNING *";
                    //TODO aggiungi timestamp?
                    let values = [res.ke, res.uid, res.ip, res.code, res.details];

                    db.getConnection(query, values, function (queryResult) {
                        if(queryResult.rowCount===1) {
                            console.log("Record inserito nel database: " + queryResult[0]);
                            resolve(queryResult.rows[0].api);
                        }
                        else {
                            reject(new Error("Errore inserimento record nel database"));
                        }
                    }).catch(function (err) {
                        throw err;
                    });
                });
            }
        }).catch((err) => {
            throw err;
        });

    });

}

function getProcessInfo(busyHost, req) {
    //4. estrai monitor id - metodo remoto
    const getProcessInfoOptions = {
        uri: busyHost + conf.shinobiNodePort + '/process/getMonitorProcessInfo',
        method: 'POST',
        json: {
            "orderBy": "cpu"
        }
    };

    return utils.handleJsonReq(getProcessInfoOptions, req.route.path);

}

function getFreeNode(controllerHost, req) {
    //TODO: modificare in POST
    const freeNodeOptions = {
        uri: controllerHost + conf.controllerPort + "/monitoring/getFreeNode",
        method: "GET"
    };

    return utils.handleJsonReq(freeNodeOptions, req.route.path);
}

function getJsonMonitor(busyHost, api, ke, mid, req) {
    const getMonitorOptions = {
        //uri: busyHost + conf.shinobiPort + "/" + api + "/monitor/" + ke + "/" + mid,
        uri: busyHost + conf.shinobiPort + createShinobiAPIUrl("getMonitor", api, ke, mid),
        method: "GET"
    };
    return utils.handleJsonReq(getMonitorOptions, req.route.path);
}

function addMonitor(freeHost, jsonMonitor, api, ke, mid, req) {
    // 7. invia monitor a nodo libero (Shinobi API)
    console.log("7 - jsonMonitor: " + jsonMonitor);
    // http://xxx.xxx.xxx.xxx/[API KEY]/configureMonitor/[GROUP KEY]/[MONITOR ID]/[ACTION*]?data={"mid":..}
    const addMonitorOptions = {
        //uri: freeHost + conf.shinobiPort + "/" + api + "/configureMonitor/" + ke + "/" + mid + "/add?data=" + JSON.stringify(jsonMonitor),
        uri: freeHost + conf.shinobiPort + createShinobiAPIUrl("addMonitor", api, ke, mid, jsonMonitor),
        method: "GET"
    };
    return utils.handleJsonReq(addMonitorOptions, req.route.path);
}

function deleteMonitor(busyHost, api, ke, mid, req) {
    // 8. stop/cancella monitor su nodo carico (Shinobi API)
    const deleteMonitorOptions = {
        //uri: busyHost + conf.shinobiPort + "/" + api + "/configureMonitor/" + ke + "/" + mid + "/delete",
        uri: busyHost + conf.shinobiPort + createShinobiAPIUrl("deleteMonitor", api, ke, mid),
        method: "GET"
    };
    return utils.handleJsonReq(deleteMonitorOptions, req.route.path);
}


/**
 * NB. NON UTILIZZATA - sostituita con funzione remota
 * Funzione che individua lo stream che sta consumando più risorse (cpu o memoria) ed estrae MonitorID (mid)
 * e Group Key (ke) dalla riga di comando del processo ffmpeg ad esso associato
 * @param orderBy
 * @returns {{ke: string, mid: string}}
 */
function getMonitorProcessInfo(orderBy) {
    /*
     * /dev\/shm\/: matches string beginning with "dev/shm/streams/"
     * (.*?) : matches any character
     * \s:	   space
     */
    var pattern = /dev\/shm\/streams(.*?)\s/;
    //TODO: cmd deve essere aggiornato per essere eguito su macchina remota
    //NB: simbolo - davanti a [%ORDERBY%] per ordine decrescente;
    //grep -ie "[f]fmpeg": permette di escludere grep dall'elenco dei processi restituiti
    var cmd = 'ps -eo pid,user,pcpu,args --sort -[%ORDERBY%] | grep -ie "[f]fmpeg" | head -n 1', param;

    switch (orderBy) {
        case 'cpu':
            param = 'pcpu';
            break;
    }

    cmd = cmd.replace('[%ORDERBY%]', param);

    //ottieni mid (Monitor ID) del processo che sta consumando più risorse
    let stdout = cp.execSync(cmd).toString();
    var result = pattern.exec(stdout)[1].split("/");

    return {ke:result[1], mid:result[2]};
}


module.exports = router;