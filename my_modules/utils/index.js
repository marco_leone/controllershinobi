const request = require('request');
const db = require('../db');
const persistBench = "false";

exports.isEmptyString = function (str) {
    return (!str || 0 === str.length);
}

exports.isEmptyObject = function (obj) {
    return !Object.keys(obj).length;
}

exports.extractHostname = function (url) {
    let hostname;
    //find & remove protocol (http, ftp, etc.) and get hostname

    if (url.indexOf("://") > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
}

exports.handleJsonReq = function (options, origin) {

    let startAt = process.hrtime();
    /*console.log("FROM: " + origin + "\n" +
        "TO: " + options.uri + "\n" +
        "Start at: " + Date.now());*/
    return new Promise(function (resolve, reject) {
        request(
            options, function (err, resp, body) {
                if(err) {
                    reject(err)
                } else {
                    exports.benchmark(startAt, null, "ms", 3, origin, options.uri, persistBench);
                    console.log("handleJsonReq - uri: " + options.uri);
                    console.log("BODY: "+ body.toString());

                    let parsed = JSON.parse(body);

                    if(typeof parsed == 'object') {
                        //è una risposta JSON
                        resolve(parsed);
                    }
                    else {
                        reject(new SyntaxError("Error in JSON.Parse"));
                    }


                }
            }
        )
    })
}

/**
 * Funzione di benchmark - stampa tempo intercorso tra l'istante in cui è stata inviata la richiesta
 * a urlReq e l'istante in cui è stata ricevuta risposta da urlResp
 * @param startAt {Array} [seconds, nanoseconds]: istante di invio richiesta (nel formato restituito da process.hrtime());
 * @param endAt {Array} [seconds, nanoseconds]: istante di fine richiesta (nel formato restituito da process.hrtime());
 * @param timeUnit: unità in cui stampare i risultati (secondi SEC - millisecondi MS - nanosecondi NS)
 * @param digits: numero di cifre da stampare in output (default: 3);
 * @param urlReq: url della richiesta
 * @param urlRes: url della risposta
 * @param persist{String} [true|false]: se true risultato è salvato nel db, altrimenti è visualizzato solo in console
 */
exports.benchmark = function (startAt, endAt, timeUnit, digits, urlReq, urlRes, persist) {
    const MS_PER_SEC = 1e3;
    const NS_PER_SEC = 1e9;
    const MS_PER_NS = 1e-6;
    const SEC_PER_NS = 1e-9;

    let diff = [];

    if (startAt !== null && endAt === null) {
        diff =  process.hrtime(startAt);
    } else if (startAt === null && endAt === null) {
        diff[0] = endAt[0] - startAt[0];
        diff[1] = endAt[1] - startAt[1];
    } else {
        console.log("Errore parametri in input");
        return;
    }


    let val;

    switch (timeUnit.toLowerCase()) {
        case 'sec' : //secondi
            val = diff[0] + diff[1]*SEC_PER_NS;
            break;
        case 'ns' : //nanosecondi
            val = diff[0]*NS_PER_SEC + diff[1];
            break;
        case 'ms': //millisecondi - default
        default:
            val = diff[0]*MS_PER_SEC + diff[1]*MS_PER_NS;
    }

    let round = digits !== undefined ? digits : 3; //default: 3
    let out = val.toFixed(round);

    console.log(`FROM UrlReq: ${urlReq} \n TO UrlRes: ${urlRes} \n ====> time: ${out} ` + timeUnit.toLowerCase());

    if(persist === "true") {
        let values = [urlReq, urlRes, out, timeUnit.toLowerCase()];
        let query = "INSERT INTO " + db.timeBenchmarkTable + "(from_url, to_url, value, unit)"
            + " VALUES ($1, $2, $3, $4) RETURNING *";

        db.getConnection(query, values, function (result) {
            if(result[0]) {
                console.log("Record inserito nel database: " + result[0]);
            }
            else {
                throw new Error("Errore inserimento record nel database");
            }
        });
    }


}
