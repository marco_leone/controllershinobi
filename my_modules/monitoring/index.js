/*
IMPLEMENTAZIONE API DI INTERFACCIA CON SISTEMA DI MONITORING PROMETHEUS
 */

const cp = require('child_process');
const conf = require('./conf.json');
//const http = require('http');
const utils = require('../utils');
const qs = require('querystring');
//const url = require('url');
const moment = require('moment');

/**
 * Avvia Prometheus - Alert Manager - Node Exporter [per TESTING LOCALE]
 */
exports.init = function () {
    //TODO: funzione di testing - rimuovere in versioni successive
    const runPrometheus = true;
    const runAlertManager = true;
    const runNodeExporter = true;
    const runDemoServices = true;

    //Run Prometheus
    if (runPrometheus) {
        const prometheus = cp.spawn("./prometheus", [], {"cwd" :conf.monitoringSystem.prometheusPath} );

        prometheus.stderr.on('Prometheus Log Error', (data) => {
            console.log(`stderr: ${data}`);
        });
        prometheus.stdout.on('Prometheus Log', (data) => {
            console.log(`stdout: ${data}`);
        });
        prometheus.on('error', function(err) {
            if (!err) {
                console.log("Prometheus started");
            }
            else {
                console.log('Prometheus spawn error: ' + err)
            };
        });
    }

    //Run AlertManager
    if (runAlertManager) {
        const am = cp.spawn("./alertmanager", [], {"cwd" :conf.monitoringSystem.alertManagerPath} );
        console.log("Alert Manager started");
        am.stderr.on('Alert Manager Log Error', (data) => {
            console.log(`stderr: ${data}`);
        });
        am.on('error', function(err) {
            console.log('Alert Manager spawn error: ' + err);
        });
    }

    if (runNodeExporter) {
        //Run NodeExporter
        const ne = cp.spawn("./node_exporter", [], {"cwd" :conf.monitoringSystem.nodeExporterPath} );
        console.log("Node Exporter started");
        ne.stderr.on('Node Exporter Log Error', (data) => {
            console.log(`stderr: ${data}`);
        });
        ne.on('error', function(err) {
            console.log('Node Exporter spawn error: ' + err);
        });
    }

    //Run DemoServices - TEST PROVVISORIO
    if(runDemoServices) {
        var demos = new Array(conf.monitoringSystem.demoServicePorts.length);
        var runDemo = function (port) {
            return cp.spawn(conf.monitoringSystem.demoServicePath, ["-listen-address=:" + port]);
        }
        conf.monitoringSystem.demoServicePorts.forEach( function(port, i) {
            demos[i] = runDemo(port);
            console.log("Demo Service on port " + port + " started");
            demos[i].stderr.on('Demo Service Log Error', (data) => {
                console.log(`stderr: ${data}`);
            });
            demos[i].on('error', function(err) {
                console.log('Demo Service error: ' + err);
            });
        });
    }
};

/*
 Ottieni nodi attivi
 */
exports.getUpNodes = function(route_response, next) {
    //TODO: provvisorio per test risposta di tipo matrix
    const query = conf.monitoringQueries.getUpNodes;
    const end = moment();

    const start = moment().subtract(30, 'seconds');
    console.log("END: " + end.toISOString());
    console.log("START: " + start.toISOString());
    const step = '15s';
    const query_str = createQueryString('range', query, '', '', start.format(), end.format(), step);

    if (utils.isEmptyString(query_str)) {
        next(new Error('Empty string in getUpNodes'));
    };

    const options = {
        baseUrl: conf.monitoringSystem.host,
        uri: conf.monitoringQueries.endpoint + query_str,
        method: "GET"
    };


    console.log("QUERY INVIATA - getUpNodes: " + options.uri);

    let resPromise = utils.handleJsonReq(options, "getUpNodes");
    resPromise.then(function (result) {
        let upNodes = parsePrometheusResponse(result, next);
        route_response.send(upNodes);
    }, function (err) {
        next(new Error(err.statusCode + ": " + err.message));
    });

}


/**
 * Determina nodo con carico minore
 * @param route_response: oggetto response
 * @param next: error handler
 */
exports.getFreeNode = function(route_response, next) {

    const query = conf.monitoringQueries.getFreeNode;
    const query_str = createQueryString('istant', query);

    if (utils.isEmptyString(query_str)) {
        next(new Error('Empty string in getFreeNode'));
    };

    const options = {
        baseUrl: conf.monitoringSystem.host,
        uri: conf.monitoringQueries.endpoint + query_str,
        method: "GET"
    };

    console.log("QUERY INVIATA - getFreeNode: " + options.uri);

    let resPromise = utils.handleJsonReq(options, "getFreeNode()");
    resPromise.then(function (result) {
        let freeNode = parsePrometheusResponse(result, next);
        route_response.send(freeNode);
        //route_response.json(freeNode);
    }, function (err) {
        next(new Error(err.statusCode + ": " + err.message));
    });

}

/*
Ottieni numero di alert generati
 */


/*** FUNZIONI PRIVATE ***/

/**
 * Crea query string per l'invocazione della API HTTP di Prometheus
 * @param {string} type = <"istant" | "range">
 * @param {string} query: Prometheus expression query string.
 * @param {string} time=<rfc3339 | unix_timestamp>: Evaluation timestamp. Optional.
 * @param {string} timeout=<duration>: Evaluation timeout. Optional. Defaults to and is capped by the value of the -query.timeout flag.
 * @param {string} start=<rfc3339 | unix_timestamp>: Start timestamp. [solo per type="range"]
 * @param {string} end=<rfc3339 | unix_timestamp>: End timestamp. [solo per type="range"]
 * @param {string} step=<duration>: Query resolution step width. [solo per type="range"]
 * @returns {string} or Error
 */
function createQueryString(type, query, time, timeout, start, end, step) {

    if (utils.isEmptyString(query)) {
        return '';
    };

    let query_obj = {'query' : query};
    let query_type;

    if (!utils.isEmptyString(timeout)) {
        query_obj.timeout = timeout;
    };
    switch (type) {
        case 'istant':
            query_type = "query";
            if (!utils.isEmptyString(time)) {
                query_obj.time = time.toString();
            }
            break;
        case 'range' :
            query_type = "query_range";
            if (!utils.isEmptyString(start)) {
                query_obj.start = start.toString();
            };
            if (!utils.isEmptyString(end)) {
                query_obj.end = end.toString();
            };
            if (!utils.isEmptyString(step)) {
                query_obj.step = step;
            }
            break;
        default:
            return '';
    }

    //esempio query: 'http://localhost:9090/api/v1/query?query=up&time=2015-07-01T20:10:51.781Z'
    return query_type + "?" + qs.stringify(query_obj);
}

/**
 * Prende in input l'oggetto JSON restituito dalla API di Prometheus ed estrae le informazioni in esso contenute
 * @param jsonResponse
 * @param next : error handler
 * @returns {Array}
 */
function parsePrometheusResponse(jsonResponse, next) {

    let parsed_result = [];

    if (jsonResponse.status === 'error') {
        next(new Error('INVALID PROMETHEUS REQUEST: ' + jsonResponse.errorType + " - " + jsonResponse.error));
    };

    let metric_data = jsonResponse.data.result; //jsonResponse.data.result: array di oggetti

    //INFO: https://prometheus.io/docs/prometheus/latest/querying/api/#expression-query-result-formats
    switch (jsonResponse.data.resultType) {
        case 'vector':
        case 'matrix':
            //per ogni DIMENSIONE (???) della metrica
            for (let i=0, l=metric_data.length; i<l; i++) {
                let parsed_obj = {};

                //AGGIUNGI LABEL
                //per ogni SOTTODIMENSIONE (???) della metrica
                for (let label in metric_data[i].metric) {

                    // check if the property/key is defined in the object itself, not in parent
                    if (metric_data[i].metric.hasOwnProperty(label)) {
                        if (label == "__name__") {
                            parsed_obj.name = metric_data[i].metric["__name__"];
                        }
                        else {
                            parsed_obj[label] = metric_data[i].metric[label];
                        }
                    }
                }

                //AGGIUNGI RISULTATI
                let results = [];
                if(jsonResponse.data.resultType == 'vector') {
                    /*
                    ogni oggetto di metric_data (= jsonResponse.data.result) contiene
                    in metric_data[i].value (= jsonResponse.data.result.value)
                    un array MONODIMENSIONALE di due elementi [<time>, <value>]
                     */
                    metric_values = metric_data[i].value;
                    results.push({"time": metric_data[i].value[0], "value":metric_data[i].value[1]});
                } else if (jsonResponse.data.resultType == 'matrix') {
                    /*
                    ogni oggetto di metric_data (= jsonResponse.data.result) contiene
                    in metric_data[i].VALUES (= jsonResponse.data.result.VALUES)
                    un array BIDIMENSIONALE in cui ogni elemento è a sua volta
                    un array di due elementi [<time>, <value>]
                     */
                    let metric_values = metric_data[i].values;
                    for (let j=0, k=metric_values.length; j<k; j++) {
                        results.push({"time": metric_values[j][0], "value": metric_values[j][1]});
                    }
                }

                parsed_obj.results = results;
                parsed_result.push(parsed_obj);
            };
            break;
        case 'scalar':
        case 'string' :
            //TODO da testare
            let results = [];
            results.push({"time": metric_data.value[0], "value":metric_data.value[1]});
            parsed_result.push({
                "type": "string",
                "results" : results
            });
            break;
        default:
            next(new Error('Query result formats not valid'));
    }

    return parsed_result;
    //return JSON.stringify({"parsed": parsed_result});


}