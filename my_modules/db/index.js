const pg = require('pg');
const config= require('../db/confDB.json');

/*var pool = new pg.Pool(config);
module.exports = pool;*/
const pool = new pg.Pool(config)

// the pool with emit an error on behalf of any idle clients
// it contains if a backend error or network partition happens
pool.on('error', (err, client) => {
    console.error('Unexpected error on idle client', err);
    process.exit(-1);
})

exports.apiTable = "public.api_cache";
exports.timeBenchmarkTable = "public.timebenchmark";

/**
 * Crea connessione con il db, prelevandola da un pool di connessione, ed esegue la query
 * @param query{string} query parametrizzata - e.g. INSERT INTO users(name, email) VALUES($1, $2) RETURNING *
 * @param values{Array} valori della query - e.g. ['mr', 'mario@rossi.com']
 * @param callback{Function} funzione eseguita in callback al termine dell'esecuzione della query
 */
exports.getConnection = function (query, values, callback) {

    return pool.connect()
        .then(client => {
            return client.query(query, values)
                .then(result => {
                    client.release()
                    //console.log(res.rows[0]);
                    callback(result);
                })
                .catch(err => {
                    client.release()
                    console.log(err.stack);
                    //TODO throw error
                    throw err;
                })
        });
}