CREATE TABLE IF NOT EXISTS public.timebenchmark (
	id int4 NOT NULL DEFAULT nextval('timebenchmark_id_seq'::regclass),
	"timestamp" timestamptz NOT NULL DEFAULT now(),
	from_url varchar NOT NULL,
	to_url varchar NULL,
	value varchar NOT NULL,
	unit varchar(10) NOT NULL,
	PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
) ;

CREATE TABLE IF NOT EXISTS public.api_cache (
	id int4 NOT NULL DEFAULT nextval('api_cache_id_seq'::regclass),
	ke varchar(50) NOT NULL,
	uid varchar(50) NOT NULL,
	ip varchar NULL,
	api varchar(100) NOT NULL,
	details text NULL,
	key_timestamp timestamptz NULL DEFAULT now(),
	PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
) ;






