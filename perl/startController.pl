#!/usr/bin/perl -w

#Usage: ./startController -c [start|stop] -i [json_config_file.json]

use warnings;
use strict;
use 5.010;
use JSON;
use Cwd;
use Getopt::Std;



# declare the perl command line flags/options we want to allow
my %options=();
getopts("c:i:", \%options); #attesi flag -c cmd -i conf.json



my $cmd = $options{c};
my $conf_file = $options{i};

if ( !($cmd eq "start" || $cmd eq "stop") ) {
	die "Command $cmd not supported. \n";
}

die "$conf_file not found!\n" if !(-e $conf_file);

open FILE, $conf_file or die "Could not open file: $!";
sysread(FILE, my $read, -s FILE);
close FILE or die "Could not close file: $!"; 

my $conf = decode_json($read);

#use Data::Dumper;
#print Dumper $conf;

my $runPrometheus = $conf->{prometheus}->{start};
my $runAlertManager = $conf->{alertManager}->{start};
my $runNodeExporter = $conf->{nodeExporter}->{start};



if ($cmd eq "start") {
	if($runPrometheus eq "true") {
		my ($dir, $exec) = ($conf->{prometheus}->{path}, $conf->{prometheus}->{executable});
		print "$conf->{prometheus}->{executable} starting... \n";
		startProcess($dir, $exec);
		print "Process $exec started!\n";
	}

	if($runAlertManager eq "true") {
		my ($dir, $exec) = ($conf->{alertManager}->{path}, $conf->{alertManager}->{executable});
		print "$conf->{alertManager}->{executable} starting... \n";
		startProcess($dir, $exec);
		print "Process $exec started!\n";
	}

	if($runNodeExporter eq "true") {
		my ($dir, $exec) = ($conf->{nodeExporter}->{path}, $conf->{nodeExporter}->{executable});
		print "$conf->{nodeExporter}->{executable} starting... \n";
		startProcess($dir, $exec);
		print "Process $exec started!\n";
	}

	1 while wait() >= 0;
}

elsif($cmd eq "stop") {
	stopProcess($conf->{prometheus}->{executable});
	stopProcess($conf->{alertManager}->{executable});
	stopProcess($conf->{nodeExporter}->{executable});
}

else {
	die "Command not supported. \n";
}










sub startProcess {
	# Get arguments
	my ($dir, $exec) = @_;

	my $process_name = $exec;
	my $find = "./";
	my $replace = "";
	$find = quotemeta $find; # escape regex metachars if present

	$process_name =~ s/$find/$replace/g;


	if(`ps cax | grep $process_name | grep -o '^[ ]*[0-9]*'`) {
		print "Process $process_name already running!\n";
	}
	else {
		my $pid = fork();
		chdir($dir) or die "Could not change current dir: $!";
		if (defined($pid) && $pid==0) {
			# background process
			exec($exec);
			die "Could not exec process: $!";
		} elsif (!defined $pid) {
			warn "Fork $exec failed: $!\n";
		}

	}


}

sub stopProcess {
	# Get arguments
	my ($process_name) = @_;

	my $find = "./";
	my $replace = "";
	$find = quotemeta $find; # escape regex metachars if present

	$process_name =~ s/$find/$replace/g;

	if(`ps cax | grep $process_name | grep -o '^[ ]*[0-9]*'`) {
		system("pkill -f $process_name") or die "Cannot kill $process_name \n";
		print "Process $process_name terminated! \n";
	}
	else {
		print "Process $process_name not running. \n";
	}

}


sub HELP_MESSAGE {
	say "Usage: ./startController -c [start|stop] -i [json_config_file.json]";
}